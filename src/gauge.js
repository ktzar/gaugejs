define([
    'jquery',
    'mustache',
    './currencyFormatter',
    './coordinates',
    'text!./gauge.svg.mustache'
], function ($, mustache, currencyFormatter, coordinates, template) {
    var WIDTH = 160,
        HEIGHT = 140,
        MARGIN = 30;

    function getCoordinatesFromValues (min, max, value) {
        var width = WIDTH - 2 * MARGIN,
            height = HEIGHT - MARGIN,
            position = coordinates.inUnitCircle(min, max, value),
            valuePercentage = (value - min) / (max - min),
            pointerX = MARGIN + (width / 2) - (width / 2) * -position.x,
            pointerY = height - (height/2 * position.y);

        return {x: pointerX, y: pointerY}
    }

    function renderData(data) {
        var value = data.value,
            min = data.min,
            max = data.max,
            coordinates = getCoordinatesFromValues(min, max, value);

        if (data.format === 'currency') {
            value = currencyFormatter.format(value, data.unit);
            min = currencyFormatter.format(min, data.unit);
            max = currencyFormatter.format(max, data.unit);
        }

        return mustache.render(template, {
            min: min,
            max: max,
            value: value,
            width: WIDTH + MARGIN,
            height: HEIGHT,
            pointerX: coordinates.x,
            pointerY: coordinates.y
        });
    }

    return {
        renderFromUrl: function (url) {
            //TODO, for the time being it fails
            return $.Deferred().reject();
        },
        renderFromData: function (data) {
            return renderData(data);
        }
    }
});
