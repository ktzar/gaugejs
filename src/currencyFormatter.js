define([], function () {
    var currencies = {
        "AED": {
            "symbol": "Dhs",
            "position": "post"
        },
        "GBP": {
            "symbol": "£",
            "position": "pre"
        },
        "USD": {
            "symbol": "$",
            "position": "pre"
        },
        "EUR": {
            "symbol": "€",
            "position": "post"
        }
    };

    return {
        format: function (value, currency) {
            var valueString = Math.round(value).toString(10),
                symbol = '',
                position = 'pre';

            if (currencies.hasOwnProperty(currency)) {
                symbol = currencies[currency].symbol;
                position = currencies[currency].position;
            }

            return position === 'pre' ?
                symbol + valueString :
                valueString + symbol;
        }
    }
});
