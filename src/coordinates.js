define([], function () {

    function roundTo2Decimals(number) {
        var rounded = Math.round(number * 100) / 100;
        //JS WTF
        if (rounded === -0) {
            return 0;
        }
        return rounded;
    }

    return {
        inUnitCircle: function (min, max, value) {
            var valuePercentage = (value - min) / (max - min),
                pointerX = - Math.cos(valuePercentage * Math.PI),
                pointerY = Math.sin(valuePercentage * Math.PI);

            return {x: roundTo2Decimals(pointerX), y: roundTo2Decimals(pointerY)}
        }
    };
});
