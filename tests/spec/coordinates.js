define(['coordinates'], function (coordinates) {
    describe('coordinates', function () {
        it('calculates correctly the position in the unit circle', function () {
            expect(coordinates.inUnitCircle(0, 100, 0)).toEqual({x: -1, y: 0});
            expect(coordinates.inUnitCircle(0, 100, 100)).toEqual({x: 1, y: 0});
            expect(coordinates.inUnitCircle(0, 100, 50)).toEqual({x: 0, y: 1});
        });
    });
});
