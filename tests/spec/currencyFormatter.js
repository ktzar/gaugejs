define(['currencyFormatter'], function (formatter) {
    describe('formatter', function () {
        it('can format dollars', function () {
            expect(formatter.format(5, 'USD')).toBe('$5');
        });

        it('can format euros', function () {
            expect(formatter.format(6, 'EUR')).toBe('6€');
        });

        it('can format decimal sterling', function () {
            expect(formatter.format(6, 'GBP')).toBe('£6');
        });

        it('can format unexisting currencies with decimals', function () {
            expect(formatter.format(6.6, 'XXX')).toBe('7');
        });

    });
});
