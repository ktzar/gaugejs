define(['jquery', 'gauge'], function ($, gauge) {
    describe('gauge', function () {
        it('it can render an svg from data', function () {
            var $markup = $(gauge.renderFromData({
                "value": 100,
                "min": 0,
                "max": 200,
                "format": "currency",
                "unit": "GBP"
            }));
            expect($markup.find('line').length).toBe(1);
            expect($markup.find('text').length).toBe(3);
            expect($markup.find('text').text()).toBe('£0£200£100');
        });

        //TODO check the angle of the line
    });
});
