var specs = [
    './spec/currencyFormatter.js',
    './spec/coordinates.js',
    './spec/gauge.js'
];

requirejs.config({
    baseUrl: "../src",
    paths: {
        'jquery': '../bower_components/jquery/dist/jquery.min',
        'mustache': '../bower_components/mustache/mustache.min',
        'text': '../bower_components/text/text',
        'jasmine': '../bower_components/jasmine-core/lib/jasmine-core/jasmine',
        'jasmine-html': '../bower_components/jasmine-core/lib/jasmine-core/jasmine-html',
        'boot': '../bower_components/jasmine-core/lib/jasmine-core/boot',
    },
    shim: {
        'jasmine': {exports: 'window.jasmineRequire'},
        'jasmine-html': {deps: ['jasmine'], exports: 'window.jasmineRequire'},
        'boot': {deps: ['jasmine', 'jasmine-html'], exports: 'window.jasmineRequire'}
    }
});

require(['boot'], function () {
    requirejs(specs, function () {
        window.onload();
    });
}); 
