## Comments
My approach to generate the gauge diagram has been using an SVG, since it's supported by IE>=9 and it's fast, parseable, and easy to test, not like HTML5's canvas.

It's also easy to get from UX / designers and make it dynamic, since it's now ubiquituous in terms of browser and UX tooling support.

This has taken me around 2.5 hours. But honestly, I don't enjoy much free time.

## How to run
Run `npm install` in the directory and then `npm run serve` to initiate a local http server on port 8080. Then to run the tests visit
`http://localhost:8080/tests`
and to see the site visit
`http://localhost:8080/site`

There are default values loaded and then a button to reload from the service provided.

## Dependencies
Defined as bower components... I don't plan to use more than jQuery for simple DOM manipulation, require for modularisation and Jasmine for testing.

## Improvements
- Use a proper currency list (https://github.com/CrowdSurge/currency-json/blob/master/currencies.json)
- Have overridable options for width and height
- Add bootstrap or something to make it nicer, but not sure what that'd prove.
- Add gradients to make the SVG nicer
- Test the mediator with spies
- Add minification to the workflow with grunt or similar
