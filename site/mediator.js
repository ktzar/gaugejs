requirejs.config({
    paths: {
        jquery: '../bower_components/jquery/dist/jquery.min',
        mustache: '../bower_components/mustache/mustache.min',
        text: '../bower_components/text/text',
    }
});
requirejs(["jquery", "../src/gauge"],
    function ($, gauge) {
        var $container;

        function renderFromData(data) {
            $container.html(gauge.renderFromData(data));
        }

        $(function () {

            $container = $('.container');
            renderFromData({
                "value": 100,
                "min": 0,
                "max": 200,
                "format": "currency",
                "unit": "GBP"
            });

            $('#reload').on('click', function () {
                $.ajax('https://widgister.herokuapp.com/challenge/frontend')
                    .done(function (data) {
                        renderFromData(data);
                    });
            });

        });
    }
);
